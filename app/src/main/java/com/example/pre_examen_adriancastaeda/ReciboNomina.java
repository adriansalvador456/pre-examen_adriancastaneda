package com.example.pre_examen_adriancastaeda;

import java.io.Serializable;
import java.util.Random;

public class ReciboNomina implements Serializable {
    private int numRecibo, puesto;
    private float horasTrabNormal, horasTrabExtras, impuestoPorc;
    private String Nombre;

    public ReciboNomina(int numRecibo, int puesto, String nombre, float horasTrabExtras,
                        float horasTrabNormal, float impuestosPorc){
          this.numRecibo = numRecibo;
          this.puesto = puesto;
          Nombre = nombre;
          this.horasTrabNormal = horasTrabNormal;
          this.horasTrabExtras = horasTrabExtras;
          this.impuestoPorc = impuestosPorc;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public float getHorasTrabNormal() {
        return horasTrabNormal;
    }

    public void setHorasTrabNormal(float horasTrabNormal) {
        this.horasTrabNormal = horasTrabNormal;
    }

    public float getHorasTrabExtras() {
        return horasTrabExtras;
    }

    public void setHorasTrabExtras(float horasTrabExtras) {
        this.horasTrabExtras = horasTrabExtras;
    }

    public float getImpuestoPorc() {
        return impuestoPorc;
    }

    public void setImpuestoPorc(float impuestoPorc) {
        this.impuestoPorc = impuestoPorc;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }
    public float calcularSubtotal(){
        float pagoInicial = 200;
        float pagoBase = 0;
        if (puesto == 1){
            pagoBase = pagoInicial*1.20f;
        }
        else if(puesto == 2){
            pagoBase = pagoInicial*1.50f;
        }
        else if(puesto == 3){
            pagoBase = pagoInicial*2f;
        }
        return (pagoBase * this.horasTrabNormal) + (pagoBase * this.horasTrabExtras * 2);
    }
    public float calcularImpuesto(){
        this.impuestoPorc = .16f;
        return calcularSubtotal()*this.impuestoPorc;
    }
    public float calcularTotal(){
        return calcularSubtotal() - calcularImpuesto();
    }
    public int numFolio(){
        Random r = new Random();
        return r.nextInt(1000);
    }

}

package com.example.pre_examen_adriancastaeda;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class ReciboNominaActivity extends AppCompatActivity {
    private ReciboNomina recibo;
    private TextView lblNumRecibo, lblNombre, lblSubtotal, lblImpuestoPor, lblTotal;
    private EditText txtHorasNormal, txtHorasExtras;
    private RadioButton rdbAuxiliar, rdbAlbañil, rdbIngObra;
    private Button btnCalcular, btnLimpiar, btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo_nomina);

        iniciarComponentes();

        // Obtiene el nombre del cliente desde el intent
        String nombreCliente = getIntent().getStringExtra("cliente");
        if (nombreCliente != null) {
            lblNombre.setText("Nombre: " + nombreCliente);
        } else {
            lblNombre.setText("Nombre: (Nombre no disponible)");
        }

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcularRecibo(nombreCliente);
            }

        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarCampos();
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    public void iniciarComponentes() {
        lblNumRecibo = findViewById(R.id.lblNumRecibo);
        lblNombre = findViewById(R.id.lblNombre);
        txtHorasNormal = findViewById(R.id.txtHorasNormal);
        txtHorasExtras = findViewById(R.id.txtHorasExtras);
        rdbAuxiliar = findViewById(R.id.rdbAuxiliar);
        rdbAlbañil = findViewById(R.id.rdbAlbañil);
        rdbIngObra = findViewById(R.id.rdbIngObra);
        lblSubtotal = findViewById(R.id.lblSubtotal);
        lblImpuestoPor = findViewById(R.id.lblImpuestoPor);
        lblTotal = findViewById(R.id.lblTotal);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnSalir = findViewById(R.id.btnSalir);
    }

    private void calcularRecibo(String nombreCliente) {
        if (txtHorasNormal.getText().toString().isEmpty()||txtHorasExtras.getText().toString().isEmpty()){
            Toast.makeText(this,"Por favor, ingresa las horas trabajadas y horas extras", Toast.LENGTH_SHORT).show();
            return;
        }
        float horasNormales = Float.parseFloat(txtHorasNormal.getText().toString());
        float horasExtras = Float.parseFloat(txtHorasExtras.getText().toString());
        int puesto = 1;

        if (rdbAuxiliar.isChecked()) {
            puesto = 1;
        } else if (rdbAlbañil.isChecked()) {
            puesto = 2;
        } else if (rdbIngObra.isChecked()) {
            puesto = 3;
        }

        String nombre = lblNombre.getText().toString().replace("Nombre: ", "");

        recibo = new ReciboNomina(0, puesto, nombre, horasExtras, horasNormales, 0);

        float subtotal = recibo.calcularSubtotal();
        float impuesto = recibo.calcularImpuesto();
        float total = recibo.calcularTotal();

        lblSubtotal.setText("Subtotal: $" + String.format("%.2f", subtotal));
        lblImpuestoPor.setText("Impuesto: $" + String.format("%.2f", impuesto));
        lblTotal.setText("Total: $" + String.format("%.2f", total));
        lblNumRecibo.setText("Numero de Recibo: " + recibo.numFolio());
    }

    private void limpiarCampos() {
        lblNumRecibo.setText("Numero de Recibo: ");
        lblNombre.setText("Nombre: ");
        txtHorasNormal.setText("");
        txtHorasExtras.setText("");
        lblSubtotal.setText("Subtotal: ");
        lblImpuestoPor.setText("Impuesto: ");
        lblTotal.setText("Total: ");
        rdbAuxiliar.setChecked(true);
    }
}
